package pro.lvlup.sdk.model.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class PaymentsListResponse {
    private int count;
    private List<PaginatedPayment> items;
}
