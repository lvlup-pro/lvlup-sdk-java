package pro.lvlup.sdk;

import pro.lvlup.sdk.exception.ErrorException;
import pro.lvlup.sdk.model.request.NewPaymentRequest;
import pro.lvlup.sdk.model.response.*;

import java.io.IOException;

public interface UpApi {
    // User
    ProfileResponse getProfileInfo() throws Exception;


    // Payments
    WalletBalanceResponse getWalletBalance() throws ErrorException, IOException;
    PaymentsListResponse getPaymentsList(int limit, int afterId, int beforeId) throws ErrorException, IOException;
    NewPaymentResponse createLinkForPayment(NewPaymentRequest newPaymentRequest) throws ErrorException, IOException;
    PaymentInfoResponse getPaymentInfo(String id) throws ErrorException, IOException;
}
