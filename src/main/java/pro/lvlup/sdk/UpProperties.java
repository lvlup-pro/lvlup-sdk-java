package pro.lvlup.sdk;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class UpProperties {

    private String artifactId;
    private String version;
    private Properties properties;

    public UpProperties() {
        try (InputStream input = UpProperties.class.getClassLoader().getResourceAsStream("lvlup-sdk-java.properties")) {
            this.properties = new Properties();
            this.properties.load(input);
        } catch (IOException e) {
            //TODO: catch error correctly
            e.printStackTrace();
        }
    }

    public String getArtifactId() {
        return this.properties.getProperty("artifactId");
    }

    public String getVersion() {
        return this.properties.getProperty("version");
    }
}
