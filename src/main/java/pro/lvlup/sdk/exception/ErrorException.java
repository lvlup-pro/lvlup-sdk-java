package pro.lvlup.sdk.exception;

public class ErrorException extends Exception {

    private int code;
    private String msg;

    public ErrorException(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
