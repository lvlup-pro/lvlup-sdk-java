package pro.lvlup.sdk.model.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class NewPaymentResponse {
    private String id;
    private String url;
}
